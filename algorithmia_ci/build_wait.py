import requests
import time


class BearerAuth(requests.auth.AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["authorization"] = "Simple " + self.token
        return r


def get_build_id(api_key, api_address, algo_path):
    url = "{}/v1/algorithms/{}/builds?limit={}".format(api_address, algo_path, 1)
    result = get_api_request(url, api_key, algo_path)
    if "results" in result:
        latest_build = result["results"][0]
        build_id = latest_build["build_id"]
        commit_sha = latest_build["commit_sha"]
        return build_id, commit_sha
    else:
        raise Exception("No builds are found for the algorithm. Will abort workflow.")


def wait_for_result(api_key, api_address, algo_path, build_id):
    waiting = True
    url = "{}/v1/algorithms/{}/builds/{}".format(api_address, algo_path, build_id)
    url_logs = "{}/v1/algorithms/{}/builds/{}/logs".format(
        api_address, algo_path, build_id
    )
    while waiting:
        result = get_api_request(url, api_key, algo_path)
        build_status = result["status"]
        print("Build status is ", build_status)
        if build_status != "in-progress":
            if build_status == "succeeded":
                waiting = False
            else:
                log_data = get_api_request(url_logs, api_key, algo_path)
                raise Exception("Build failure:\n{}".format(log_data["logs"]))
        else:
            time.sleep(5)


def get_api_request(url, api_key, algo_path):
    response = requests.get(auth=BearerAuth(api_key), url=url)
    if response.status_code == 200:
        return response.json()
    elif response.status_code == 404:
        raise Exception("Check 'algo_path' {}, 404 not found".format(algo_path))
    elif response.status_code == 401:
        raise Exception("Check 'mgmt_api_key' {}, 401 not authorized".format(api_key))
    else:
        raise Exception("request failed with status: {}".format(response.status_code))


def build_wait(mgmt_key, api_address, algo_path):
    print("--- Finding build in progress ---")
    build_id, commit_sha = get_build_id(mgmt_key, api_address, algo_path)
    print("--- Build ID found, waiting for result ---")
    wait_for_result(mgmt_key, api_address, algo_path, build_id)
    print("--- Build successful ---")
    return commit_sha
