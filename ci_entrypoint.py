#!/usr/bin/python3

import os, json, urllib
from urllib import parse
from algorithmia_ci import build_wait, test_algo, publish_algo


ALGO_TESTS_PATH = "./TEST_CASES.json"

if __name__ == "__main__":
    algo_username = os.getenv("ALGO_USER")
    algo_name = os.getenv("ALGO_NAME")
    api_address = os.getenv("API_ADDRESS")
    api_key = os.getenv("API_KEY")
    api_key = parse.unquote_plus(api_key)

    publish_schema = os.getenv("ALGO_PUBLISH_SCHEMA")

    # TODO: Add validations
    # if not api_key:
    #     raise Exception("field 'api_key' not defined in workflow")
    # if not api_address:
    #     raise Exception("field 'api_address' not defined in workflow")
    # if not publish_schema:
    #     raise Exception("field 'version_schema' not defined in workflow")
    # if not repo_name:
    #     raise Exception("field 'repo_name' not defined in workflow")

    # TODO: Make algo schema minor by default, warn the user if not set or outside these
    # if algo_schema not in ["major", "minor", "revision"]:
    # raise Exception(
    #     "{} is not considered a valid algorithm version schema".format(algo_schema)
    # )

    # TODO: formatlari kaldir fstring yap

    algo_path = f"{algo_username}/{algo_name}"
    algo_hash = build_wait(api_key, api_address, algo_path)
    if os.path.exists(ALGO_TESTS_PATH):
        with open("./TEST_CASES.json") as f:
            case_data = json.load(f)
            test_algo(api_key, api_address, case_data, algo_path, algo_hash)
    else:
        print(
            "No test cases found at {}. Continuing with publishing without testing.".format(
                ALGO_TESTS_PATH
            )
        )
    publish_algo(api_key, api_address, publish_schema, algo_path, algo_hash)

